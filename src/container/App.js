import React from 'react';
import "../components/Card"
import CardList from '../components/CardList';
import SearchBox from '../components/SearchBox';
import Scroll from '../components/Scroll'
import { LoopCircleLoading } from 'react-loadingg';
import './App.css'

class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      awaitToRender : true,
      pokemons: null,
      searchField: ''
    };
  }

  async componentDidMount() {
    const pokeURL = "https://pokeapi.co/api/v2/pokemon?limit=350";
    let responsePokemons = await fetch(pokeURL);
    let dataPokemonsJson = await responsePokemons.json();
    let pokemons = [];

    const getDataByPokemon = async (pokemonUrl) => {
      let responseByPokemon = await fetch(pokemonUrl);
      let dataByPokemon = await responseByPokemon.json();
      return dataByPokemon;
    }

    for (const result of dataPokemonsJson.results) {
      let dataPokemon = await getDataByPokemon(result.url);
      pokemons.push({ name: result.name, info: dataPokemon });
    }

    this.setState({ pokemons: pokemons, awaitToRender : false })
  }

  onSearchChange = (event) => {
    this.setState({ searchField: event.target.value })
  }

  render() {
    let awaitToRender = this.state.awaitToRender; 
    let filteredPokemons = [];

    if(!awaitToRender) {
      filteredPokemons = this.state.pokemons.filter(pokemon => {
        return pokemon.name.toLowerCase().includes(this.state.searchField.toLowerCase())
      });
    }
    
    return (
      <div className="tc">
        <div>
          <h1 className="f1">PokeDex</h1>
          <div className="box" style={{ width: "100px", height: "100px" }}></div>
        </div>
        {awaitToRender ? 
          (
          <div>
            <h1>Building your pokedex</h1>
            <LoopCircleLoading />
          </div>
          ) : 
          (
            <div>
              <h2 style={{color:"white"}}>Pokemon founds: {filteredPokemons.length}</h2>
              <SearchBox searchChange={this.onSearchChange}></SearchBox>
              <Scroll>
                <CardList pokemons={filteredPokemons}></CardList>
              </Scroll>
            </div>
          )}

      </div>

    );
  }
}

export default App;
