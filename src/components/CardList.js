import React from "react";
import Card from "./Card"

const CardList = ({ pokemons }) => {
    const cardComponent = pokemons.map((pokemon, index) => {
        return <Card key={index} name={pokemon.name} urlImage={pokemon.info.sprites}></Card>
    });

    return(
        <div>
            {cardComponent}
        </div>
    )
}

export default CardList;