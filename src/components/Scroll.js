import React from 'react';

const Scroll = (props) => {
    return (
        <div style={{overflow: 'scroll', border:"6px", height: "600px", color: "black"}}>
            {props.children}
        </div>
    );

}

export default Scroll;