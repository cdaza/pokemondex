import React from "react";

const Card = ({name, urlImage}) => {
    return (
        <div className="bg-light-red dib br3 pa3 ma2 grow bw2 shadow-5">
            <img alt="pokemon" src={urlImage.front_default} />
            <img alt="pokemon" src={urlImage.back_default} />
            <div>
                <h2>{name.charAt(0).toUpperCase() + name.slice(1)}</h2>
            </div>
        </div>
    );
}

export default Card;